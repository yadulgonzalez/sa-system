#Toc Toc Servicios Cuba 

###Configuración Base de Datos

Ejecutar la secuencia de commando siguientes:

* Borrar Base de Datos (si existe)
```
php bin/console d:d:d --force
 ```
 
* Crear Base de Datos
```
php bin/console d:d:c    
``` 

* Crear Esquema de Tablas en la Base de Datos
```
php bin/console d:s:c 
```

* Actualizar Esquema de Tablas en la Base de Datos (si existe)
```
php bin/console d:s:u --force 
```

* Poblar Base de Datos con datos de prueba
```
php bin/console d:f:l
```

Una vez que se incluyan los usuarios de prueba, podrá usarse los siguientes usuarios:

Con ROLE_ADMIN    
```
usuario: admin
contraseña: admin
```

Con ROLE_CLIENT    
```
usuario: client
contraseña: client
```

Con ROLE_USER    
```
usuario: user1
contraseña: user1
```
```
usuario: user2
contraseña: user2
```

Con la siguiente url podrás tener acceso al Backend 
```
http://localhost/toctoc/public/admin
```

###Sobre la API

Las entidades serán expuestas mediante el uso del paquete API Platform. Todas las operaciones CRUD podrán ser expuestas 
con el uso de los métodos: **GET, POST, PUT, PATCH y DELETE**.

Cada una de las peticiones a la API se harán con la autorización de tipo "Bearer Token" usando el token asociado al
usuario que realiza la petición, siempre que el mismo esta habilitado en el Sistema. De no ser así las peticiones no 
tendrán permisos y serán rechazadas.

**Importante:** Sólo las rutas para la API contendrán de manera obligatoria el texto "api" como parte de su nombre.

El siguiente ejemplo muestra una petición GET para una colección, usando "curl" en el lenguage PHP:

```
<?php

$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => "http://localhost/toctoc/public/api/services",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 0,
  CURLOPT_FOLLOWLOCATION => true,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "GET",
  CURLOPT_HTTPHEADER => array(
    "Authorization: Bearer c8a12e38b89c94ac5da61d64f64b34a175aa04ab1fefb5f44fa2024dbacdcc6c660877dbcce912eaf153fb14532290f61208ef2e3921ea2477812384",
    "Cookie: PHPSESSID=4p8gmk6k04kr2050bq3eqed7r5"
  ),
));

$response = curl_exec($curl);

curl_close($curl);
echo $response;
```


###Sobre el uso de los Data Transformer Objects (DTO) de API Platform

Las entidades que exponen campos que requieren de alguna transformación, usarán las siguientes anotaciones, como se 
muestra para la entidad User 

```
 * @ApiResource(
 *     collectionOperations={
 *         "get"={
 *             "output"=UserOutput::class
 *         }
 *     },
 *     itemOperations={
 *         "get"={
 *             "output"=UserOutput::class
 *         }
 *     }
 * )
 */
 class User implements UserInterface
 {
   ...
 }
 ```
 
 En la carpeta *DTO* se creará una clase "output", en este caso *UserOutput* que tendrá los campos que serán expuestos una 
 vez que se realice la transformacion.
  
```
<?php

namespace App\DTO;

final class UserOutput
{
    /**
     * @var integer
     */
    public $id;

    /**
     * @var string
     */
    public $email;

    ...
}      
```

Luego se creará en la carpeta *DataTransformer* la clase *UserOutputDataTransformer* que tiene la responsabilidad de 
aplicar las transformaciones en su método *transform*.  

En el siguiente ejemplo, el campo *image* de la entidad *User* será transformado. 

```
<?php

namespace App\DataTransformer;

use ApiPlatform\Core\DataTransformer\DataTransformerInterface;
use App\DTO\UserOutput;
use App\Entity\User;

final class UserOutputDataTransformer implements DataTransformerInterface
{
    /**
     * {@inheritdoc}
     */
    public function transform($data, string $to, array $context = [])
    {
        $output = new UserOutput();
        $output->id = $data->getId();
        $output->email = $data->getEmail();
        $output->first_name = $data->getFirstName();
        $output->last_name = $data->getLastName();
        $output->image = 'data='. base64_encode('/public/images/users/profile/' . $data->getImage());

        return $output;
    }

    /**
     * {@inheritdoc}
     */
    public function supportsTransformation($data, string $to, array $context = []): bool
    {
        return UserOutput::class === $to && $data instanceof User;
    }
}
```

Y por último se habilitará la clase antes creada como servicio en el Sistema.

```
services:
    App\DataTransformer\UserOutputDataTransformer: ~
        # Uncomment only if autoconfiguration is disabled
        #tags: [ 'api_platform.data_transformer' ]
```

 
**Grupo PharesSoft**
 
 
 
 
 
 
