<?php

include_once(dirname(__FILE__) . '/../../connect.php');

$container->setParameter('DB_HOST', DB_HOST);
$container->setParameter('DB_NAME', DB_NAME);
$container->setParameter('DB_USER', DB_USER);
$container->setParameter('DB_PASSWORD', DB_PASSWORD);
$container->setParameter('DB_PORT', DB_PORT);


$container->setParameter('EMAIL_MAIN', EMAIL_MAIN);
$container->setParameter('EMAIL_HOST', EMAIL_HOST);
$container->setParameter('EMAIL_PORT', EMAIL_PORT);
$container->setParameter('EMAIL_USER', EMAIL_USER);
$container->setParameter('EMAIL_PASSWORD', EMAIL_PASSWORD);

?>
