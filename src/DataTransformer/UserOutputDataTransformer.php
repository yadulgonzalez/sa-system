<?php

/**
 * Created by PhpStorm.
 * User: TONYSOFT
 * Date: 12/09/2020
 * Time: 22:55
 */

namespace App\DataTransformer;

use ApiPlatform\Core\DataTransformer\DataTransformerInterface;
use App\DTO\UserOutput;
use App\Entity\User;

final class UserOutputDataTransformer implements DataTransformerInterface
{
    /**
     * {@inheritdoc}
     */
    public function transform($data, string $to, array $context = [])
    {
        $output = new UserOutput();
        $output->id = $data->getId();
        $output->email = $data->getEmail();
        $output->first_name = $data->getFirstName();
        $output->last_name = $data->getLastName();
        $output->image = base64_encode('/public/images/users/profile/' . $data->getImage());

        return $output;
    }

    /**
     * {@inheritdoc}
     */
    public function supportsTransformation($data, string $to, array $context = []): bool
    {
        return UserOutput::class === $to && $data instanceof User;
    }
}