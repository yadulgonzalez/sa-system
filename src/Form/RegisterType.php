<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;

class RegisterType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('name', TextType::class, ['attr' => [
                        'placeholder' => 'Nombre completo',
                        'class' => 'form-control',
                        'required' => 'required',
                        'autofocus' => 'autofocus']])
                ->add('email', EmailType::class, ['attr' => [
                        'placeholder' => 'Email',
                        'class' => 'form-control',
                        'required' => 'required']])
                ->add('email2', EmailType::class, ['attr' => [
                        'placeholder' => 'Confirme su email',
                        'class' => 'form-control',
                        'required' => 'required']])
                ->add('password', PasswordType::class, ['attr' => [
                        'placeholder' => 'Contraseña',
                        'class' => 'form-control',
                        'required' => 'required']])
                ->add('password2', PasswordType::class, ['attr' => [
                        'placeholder' => 'Confirme su contraseña',
                        'class' => 'form-control',
                        'required' => 'required']])
        ;
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults([
                // Configure your form options here
        ]);
    }

}
