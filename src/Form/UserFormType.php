<?php

namespace App\Form;

use App\Entity\Municipal;
use App\Entity\Province;
use App\Entity\User;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $is_new = empty($builder->getData()->getId()) ? true : false;

        if ($options['user'] || $options['editing'])
        {
            $builder
                ->add('email', EmailType::class, [
                    'help' => 'Correo electrónico del user'
                ]);
        }

        $builder
            ->add('first_name', TextType::class, [
                'label' => 'Nombre(s)',
                'help' => 'Nombre(s) del usuario',
            ])
            ->add('last_name', TextType::class, [
                'label' => 'Apellido(s)',
                'help' => 'Apellido(s) del usuario'
            ])
            ->add('username', TextType::class, [
                'label' => 'Usuario',
                'help' => 'Identifica el usuario'
            ])
            ->add('file_image', FileType::class, [
                'label' => 'Imagen (160*160px)',
                'help' => 'Avatar del usuario (160*160px)',
                'mapped' => false,
                'required' => false,
            ]);

        if ($options['profile'])
        {
            $builder
                ->add('enabled', HiddenType::class,[
                    'label' => false,
                ])
                ->add('roles', HiddenType::class, [
                    'mapped' => false,
                    'label' => false,
                ]);
        }
        elseif($options['editing'])
        {
            $builder
                ->add('enabled', ChoiceType::class, [
                    'help' => 'Estado del user',
                    'choices' => [
                        'Activo' => '1',
                        'Inactivo' => '0',
                    ],
                    'label' => 'Estado'

                ])
                ->add('roles', ChoiceType::class, [
                    'help' => 'Define el rol del user',
                    'choices' => [
                        'ROLE_USER' => 'ROLE_USER',
                        'ROLE_ADMIN' => 'ROLE_ADMIN',
                    ],
                    'mapped' => false
                ]);
        }
        elseif ($options['client'])
        {
            $builder
                ->add('identity', TextType::class, [
                    'label' => 'Carné de identidad',
                    'help' => 'Número del carné de identidad'
                ])
                ->add('identity_image_file', FileType::class, [
                    'label' => 'Foto del Carné de identidad',
                    'mapped' => false,
                ]);
        }

        $builder
            ->add('password', PasswordType::class, [
                'label' => 'Contraseña',
            ])
            ->add('province', EntityType::class, [
                'label' => 'Provincia',
                'class' => Province::class,
                'query_builder' => function (EntityRepository $entity_repository) {
                    return $entity_repository->createQueryBuilder('p')
                        ->select('p')
                        ->orderBy('p.name', 'ASC');
                },
                'placeholder' => 'Seleccione la provincia'
            ])
            ->add('municipal', EntityType::class, [
                'label' => 'Municipio',
                'class' => Municipal::class,
                'query_builder' => function (EntityRepository $entity_repository) {
                    return $entity_repository->createQueryBuilder('m')
                        ->select('m')
                        ->orderBy('m.name', 'ASC');
                },
                'attr' => $is_new ? ['disabled' => 'disabled'] : [],
                'placeholder' => 'Seleccione el municipio'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
            'user' => null,
            'editing' => null,
            'profile' => null,
            'client' => null,
        ]);
    }


}