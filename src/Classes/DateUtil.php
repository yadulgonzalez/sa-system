<?php

namespace App\Classes;

/**
 * Description of DateUtil
 *
 * @author yadul
 */
class DateUtil {

    public static function getTodayExactDate() {

        return date_create_from_format("Y-m-d", date("Y-m-d"));
    }

}
