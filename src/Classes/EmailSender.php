<?php
/**
 * Created by PhpStorm.
 * User: TONYSOFT
 * Date: 20/09/2020
 * Time: 15:04
 */
namespace App\Classes;

use App\Repository\ConfigRepository;
use Psr\Container\ContainerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;

class EmailSender
{

    protected static $container;
    protected static $parameters;
    protected static $configRepository;

    /**
     * @param ContainerInterface $container
     * @param ContainerBagInterface $bag
     * @param ConfigRepository $configRepository
     */
    public function start(ContainerInterface $container, ContainerBagInterface $bag, ConfigRepository $configRepository)
    {
        self::$container = $container;
        self::$parameters = $bag;
        self::$configRepository = $configRepository;
    }

    /**
     * @param ContainerInterface $container
     * @param ContainerBagInterface $bag
     * @param ConfigRepository $configRepository
     * @param null $from
     * @param $to
     * @param $subject
     * @param $body
     * @param null $attachment
     */
    public static function sendEmail(ContainerInterface $container, ContainerBagInterface $bag, ConfigRepository $configRepository, $from = null, $to, $subject, $body, $attachment = null)
    {
        self::start($container, $bag, $configRepository);

        if (empty($from))
        {
            if (!empty($config = self::$configRepository->findOneBy(["name" => "main_email"])))
            {
                $from = $config->getContent();
            }
            else
            {
                $from = self::$parameters->get('EMAIL_MAIN');
            }
        }

        try
        {
            // Create the Transport
            $transport = (new \Swift_SmtpTransport(self::$parameters->get('EMAIL_HOST'), self::$parameters->get('EMAIL_PORT')))
                ->setUsername(self::$parameters->get('EMAIL_USER'))
                ->setPassword(self::$parameters->get('EMAIL_PASSWORD'));

            if ($attachment)
            {
                $email_attachment = \Swift_Attachment::fromPath($attachment)->setDisposition('inline');
            }

            // Create a message
            $message = new \Swift_Message();
            $message
                ->setSubject($subject)
                ->setFrom($from)
                ->setTo($to)
                ->setBody($body, 'text/html');

            if ($attachment)
            {
                // Attach it to the message
                $message->attach($email_attachment);
            }

            // Create the Mailer using your created Transport
            $mailer = new \Swift_Mailer($transport);
            // Send the message
            $mailer->send($message);
        }
        catch (\Exception $e)
        {
            error_log($e->getMessage());
        }
    }

}