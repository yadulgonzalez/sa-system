<?php

/**
 * Description of DebugController
 *
 * @author yadul
 */

namespace App\Classes;

class DebugUtil {

    const DEV_MODE_EXPORT = true;
    const DEV_MODE_CONSOLE = true;
    const DEF_FILE = "./debug.log";

    public static function dump($title, $data = NULL) {

        if (DebugUtil::DEV_MODE_CONSOLE) {

            echo "<b>$title</b>";
            echo "<br>";
            if ($data) {
                var_dump($data);
                echo "<br>";
            } else {
                echo "NULL<br>";
            }
        }
    }

    public static function console($title, $data = NULL) {

        if (DebugUtil::DEV_MODE_CONSOLE) {

            if (is_object($title) || is_array($title) || is_resource($title)) {
                $title = print_r($title, true);
            }

            echo (!empty($data) ? "\n*** $title ***\n" : $title);

            if (is_object($data) || is_array($data) || is_resource($data)) {
                $data = print_r($data, true);
            }

            if ($data) {

                echo "$data\n";
            } else {

                echo "\n";
            }
        }
    }

    public static function export($data = "NULL") {

        if (DebugUtil::DEV_MODE_EXPORT) {

            if ($data != "NULL") {

                $file = fopen(DebugUtil::DEF_FILE, 'a');
                fwrite($file, date("Y-m-d H:i:s") . " ---------------------\n");

                if (is_object($data) || is_array($data) || is_resource($data)) {
                    $data = print_r($data, true);
                }

                fwrite($file, wordwrap($data, 80) . "\n");

                fclose($file);
            }
        }
    }

}
