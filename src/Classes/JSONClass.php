<?php

namespace App\Classes;

/**
 * Description of JSONClass
 *
 * @author Yadul
 */
class JSONClass {

    private $my_class_name;

    public function __construct() {

        $this->my_class_name = get_class($this);
    }

    public function toJSON() {

        return json_encode($this);
    }

    public function fromJSON($jsonString) {

        $this->fromArray(json_decode($jsonString));
    }

    public function fromArray($data) {

        try {
            foreach ($data as $key => $val) {

                if (property_exists($this->my_class_name, $key)) {
                    $this->$key = $val;
                }
            }
        } catch (Exception $ex) {

        }
    }

    public function fromObject($object) {

        $class_name = get_class($object);

        $vars = get_class_vars($class_name);

        foreach ($vars as $key => $val) {

            if (property_exists($this->my_class_name, $key)) {
                $this->$key = $val;
            }
        }
    }

}
