<?php

namespace App\Controller\Backend;

use App\Entity\Config;
use App\Form\ConfigType;
use App\Repository\ConfigRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * @Route("/admin/configuracion")
 * @IsGranted("ROLE_ADMIN")
 */
class ConfigController extends BaseController
{
    /**
     * @Route("/", name="config_index", methods={"GET"})
     *
     * @param ConfigRepository $configRepository
     * @return Response
     */
    public function index(ConfigRepository $configRepository): Response
    {
        return $this->render('backend/config/index.html.twig', [
            'configs' => $configRepository->findAll(),
            'page_active' => 'config',
        ]);
    }

    /**
     * @Route("/nueva", name="config_new", methods={"GET","POST"})
     *
     * @param Request $request
     * @return Response
     */
    public function new(Request $request): Response
    {
        $config = new Config();
        $form = $this->createForm(ConfigType::class, $config);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            $this->getEM()->persist($config);
            $this->getEM()->flush();
            $this->addFlash('success', 'La variable de configuración ' . $config->getName() . ' fue creada exitosamente');

            return $this->redirectToRoute('config_index');
        }

        return $this->render('backend/config/new.html.twig', [
            'config' => $config,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/editar", name="config_edit", methods={"GET","POST"})
     *
     * @param Request $request
     * @param Config $config
     * @return Response
     */
    public function edit(Request $request, Config $config): Response
    {
        $form = $this->createForm(ConfigType::class, $config);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            $this->getEM()->flush();
            $this->addFlash('success', 'La variable de configuración ' . $config->getName() . ' fue actualizada exitosamente');

            return $this->redirectToRoute('config_index');
        }

        return $this->render('backend/config/edit.html.twig', [
            'config' => $config,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="config_delete", methods={"GET"})
     *
     * @param Request $request
     * @param Config $config
     * @return Response
     */
    public function delete(Request $request, Config $config): Response
    {
        $this->getEM()->remove($config);
        $this->getEM()->flush();
        $this->addFlash('success', 'La variable de configuración ' . $config->getName() . ' fue eliminada exitosamente');

        return $this->redirectToRoute('config_index');
    }
}
