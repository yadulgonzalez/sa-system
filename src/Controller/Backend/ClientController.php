<?php

namespace App\Controller\Backend;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Entity\Plan;
use App\Repository\PlanRepository;
use App\Entity\Contract;
use App\Repository\ContractRepository;
use App\Repository\TransactionRepository;
use App\Repository\AccountRepository;
use App\Classes\StringUtil;

/**
 * Class DashboardController
 * @package App\Controller
 * @Route("/members")
 */
class ClientController extends BaseController {

    /**
     * @Route("/", name="client_start")
     */
    public function start() {
        return $this->redirectToRoute("client_dashboard");
    }

    /**
     * @Route("/dashboard", name="client_dashboard")
     */
    public function index(TransactionRepository $transactionRepository) {

        if (!$this->is_client()) {
            return $this->redirectToRoute('logout');
        }

        return $this->render('backend/client/dashboard.html.twig', [
                    'active' => 'members',
                    'subactive' => 'dashboard',
                    'backend' => true,
                    'account' => $this->getUser()->getAccount(),
                    'lastTransactions' => $transactionRepository->findLastRecord(5)
        ]);
    }

    /**
     * @Route("/contract", name="client_contract")
     */
    public function newContract(PlanRepository $planRepository) {
        if (!$this->is_client()) {
            return $this->redirectToRoute('logout');
        }

        $plans = $planRepository->findAllOrdered();

        return $this->render('backend/client/contract.html.twig', [
                    'active' => 'members',
                    'subactive' => 'contract',
                    'backend' => true,
                    'plans' => $plans,
                    'colors' => Plan::COLORS
        ]);
    }

    /**
     * @Route("/contract/payment", name="client_contract_payment")
     */
    public function newContractPayment(Request $request, PlanRepository $planRepository) {
        if (!$this->is_client()) {
            return $this->redirectToRoute('logout');
        }

        $planVar = $request->request->get('drop');

        $planId = substr($planVar, 2);

        return $this->render('backend/client/contract_payment.html.twig', [
                    'active' => 'members',
                    'subactive' => 'contract',
                    'backend' => true,
                    'plan' => $planRepository->find($planId),
                    'colors' => Plan::COLORS,
                    'account' => $this->getUser()->getAccount()
        ]);
    }

    /**
     * @Route("/contract/payment/balance", name="client_contract_payment_balance")
     */
    public function newContractPaymentBalance(Request $request, PlanRepository $planRepository,
            ContractRepository $contractRepository, TransactionRepository $transactionRepository,
            AccountRepository $accountRepository) {
        if (!$this->is_client()) {
            return $this->redirectToRoute('logout');
        }

        $planVar = $request->request->get('drop');

        $planId = substr($planVar, 2);

        $plan = $planRepository->find($planId);

        $code = $contractRepository->generateNextCode();

        $account = $this->getUser()->getAccount();

        $contract = new Contract($code, $account, $plan, 'balance', 'USD');

        $this->getEM()->persist($contract);
        $this->getEM()->flush();

        $transactionRepository->create($account, 0, $plan->getValue(), 'Contrato ' . $contract->getCode() . ' (' . $plan->getCompleteName() . ') con saldo interno');

        $accountRepository->update($account, 0, $plan->getValue());

        $this->addFlash('success', 'Plan ' . $plan->getName() . ' ' . StringUtil::numberToRoman($plan->getLevel()) . ' adquirido con éxito');

        return $this->redirectToRoute('client_dashboard');
    }

    /**
     * @Route("/account/update", name="client_account_update")
     */
    public function accountUpdate() {
        if (!$this->is_client()) {
            return $this->redirectToRoute('logout');
        }

        $account = $this->getUser()->getAccount();
        
        $this->getEM()->getRepository('App:Account')->update($account);

        return new Response('Ok');
    }

}
