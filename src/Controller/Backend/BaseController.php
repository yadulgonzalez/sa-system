<?php

namespace App\Controller\Backend;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\Exception\FileNotFoundException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpKernel\KernelInterface;

abstract class BaseController extends AbstractController
{
    private $em;
    protected $dir_to_save = '/public/assets/images';
    private $kernel;

    public function __construct(EntityManagerInterface $em, KernelInterface $kernel)
    {
        $this->em = $em;
        $this->kernel = $kernel;
    }

    /**
     * @return User|null
     */
    protected function getUser(): ?User
    {
        return parent::getUser();
    }

    /**
     * @return EntityManagerInterface
     */
    public function getEM(): EntityManagerInterface
    {
        return $this->em;
    }

    /**
     * @param EntityManagerInterface $em
     */
    public function setEM(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @return KernelInterface
     */
    public function getKernel(): KernelInterface
    {
        return $this->kernel;
    }

    /**
     * @param KernelInterface $kernel
     */
    public function setKernel(KernelInterface $kernel)
    {
        $this->kernel = $kernel;
    }


    /**
     * This function check if exist the path, if not exist create it and move the file to the destination defined
     *
     * @param $file
     * @param null $file_name
     * @param bool $set_time_in_name
     * @return bool|null|string
     */
    public function upload($file, $file_name = null, $set_time_in_name = false)
    {
        // Check if exist directory, if not exist create it
        if (!is_dir($this->kernel->getProjectDir() . $this->dir_to_save)) {
            $directory = $this->kernel->getProjectDir();

            $name_dirs_to_create = explode('/', $this->dir_to_save);
            foreach ($name_dirs_to_create as $dir)
            {
                $directory .= $dir;
                if (!is_dir($directory))
                {
                    mkdir($directory);
                }
                $directory .= '/';
            }
        }

        // If not exit file to upload and the file name has assigned value
        if
        (
            empty($file)
            &&
            !empty($file_name)
        )
        {
            try
            {
                if (is_file($this->kernel->getProjectDir(). $this->dir_to_save. $file_name))
                {
                    $file = new UploadedFile($this->kernel->getProjectDir() . $this->dir_to_save, $file_name);
                }
                else
                {
                    throw new FileNotFoundException();
                }
            }
            catch (FileNotFoundException $e)
            {
                error_log( $e->getMessage() );
            }
            catch (FileException $e)
            {
                error_log( $e->getMessage() );
            }

        }

        //Not empty file to upload
        if ( !empty( $file ) )
        {
            //Get the name of file
            $name = $set_time_in_name ? time() . "_" . $file->getClientOriginalName() : $file->getClientOriginalName();
            // Construct the route to save the file
            $dir = $this->kernel->getProjectDir() . $this->dir_to_save;
            //Move the file to the constructed route
            $file->move($dir, $name);

            return $name;
        }

        return false;
    }

    /**
     * This function converts an array in a string separates by the parameter received
     *
     * @param array $array_in
     * @param string $separator
     * @return bool|string
     */
    public function array_to_string(Array $array_in, string $separator = '|')
    {
        $string_result = '';

        for ( $i = 0; $i < count($array_in); $i ++ )
        {
            $string_result .= $array_in[$i] . $separator;
        }

        return !empty($string_result) ? substr( $string_result, 0, strlen($string_result) - 1 ) : '';
    }


    /**
     * Removes image from a path
     *
     * @param string $path
     * @param string $image
     */
    public function remove_image(string $path = "/public/assets/images/", string $image)
    {
        $file_name = $this->kernel->getProjectDir() . $path . $image;
        if (is_file($file_name))
        {
            unlink($file_name);
        }
    }

    /**
     * Returns TRUE if is a client, otherwise returns FALSE
     *
     * @return bool
     */
    public function is_client()
    {

        return in_array('ROLE_CLIENT', $this->getUser()->getRoles());
    }

    /**
     * Returns TRUE if is administrator, otherwise returns FALSE
     *
     * @return bool
     */
    public function is_admin()
    {

        return in_array('ROLE_ADMIN', $this->getUser()->getRoles());
    }

    /**
     * Returns TRUE if is inactive client, otherwise returns FALSE
     *
     * @return bool
     */
    public function is_inactive_client()
    {

        return in_array('ROLE_CLIENT', $this->getUser()->getRoles()) && !$this->getUser()->getReady();
    }

}