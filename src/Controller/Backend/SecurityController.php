<?php

namespace App\Controller\Backend;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use App\Entity\User;
use App\Entity\Account;
use App\Entity\Register;
use App\Form\RegisterType;
use App\Repository\UserRepository;

/**
 * Class SecurityController
 * @package App\Controller
 */
class SecurityController extends AbstractController {

    /**
     * @param AuthenticationUtils $authenticationUtils
     * @return Response
     * @Route("/login", name="app_login")
     */
    public function login(AuthenticationUtils $authenticationUtils): Response {
        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('backend/security/login.html.twig', [
                    'last_username' => $lastUsername,
                    'error' => $error,
                    'active' => 'security'
        ]);
    }

    /**
     * @Route("/register/{sponsor_code}", name="app_register_with_sponsor")
     */
    public function registerWithSponsor($sponsor_code, Request $request, UserPasswordEncoderInterface $encoder,
            UserRepository $userRepository): Response {

        if (!empty($sponsor = $userRepository->findOneBy(['sponsorCode' => $sponsor_code]))) {
            $request->getSession()->set('sponsor_id', $sponsor->getId());
        }

        return $this->register($request, $encoder, $userRepository);
    }

    /**
     * @Route("/register", name="app_register")
     */
    public function register(Request $request, UserPasswordEncoderInterface $encoder, UserRepository $userRepository): Response {

        $register = new Register();
        $form = $this->createForm(RegisterType::class, $register);
        $form->handleRequest($request);

        $em = $this->getDoctrine()->getManager();

        if ($form->isSubmitted() && $form->isValid()) {

            $user = new User();
            $user->setFirstName($register->getName());
            $user->setEmail($register->getEmail());
            $user->setUsername($register->getEmail());
            $user->setPassword($encoder->encodePassword($user, $register->getPassword()));
            $user->setRoles(['ROLE_CLIENT']);
            $user->setEnabled(true);
            $user->setCreatedAt(new \DateTime());

            if (!empty($sponsorId = $request->getSession()->get('sponsor_id'))) {
                $user->setMySponsor($userRepository->find($sponsorId));
            }

            $em->persist($user);
            $em->flush();

            $user->setSponsorCode(strlen($user->getEmail()) . $user->getId());
            $em->flush();

            $account = new Account($user);
            $em->persist($account);
            $em->flush();

            $this->addFlash('success', 'Se ha registrado exitosamente. Enviamos un mensaje para confirmar su cuenta de email.');

            return $this->redirectToRoute('app_login');
        }

        return $this->render('backend/security/register.html.twig', [
                    'form' => $form->createView(),
                    'active' => 'security'
        ]);
    }

    /**
     * @Route("/logout", name="app_logout")
     */
    public function logout() {

    }

}
