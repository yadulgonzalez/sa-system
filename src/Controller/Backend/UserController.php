<?php

namespace App\Controller\Backend;

use App\Entity\User;
use App\Form\UserFormType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * Class UserController
 * @Route("/admin/usuarios")
 * @@IsGranted({"ROLE_ADMIN","ROLE_CLIENT"}))
 * @package App\Controller
 *
 */
class UserController extends BaseController
{

    /**
     * @Route("/", name="app_user")
     */
    public function index()
    {
        return $this->render('backend/user/index.html.twig', [
            'users' => $this->getEM()->getRepository(User::class)->findAll(),
        ]);
    }

    /**
     * @Route("/nuevo", name="app_new_user")
     * @param Request $request
     * @param UserPasswordEncoderInterface $password_encoder
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function new(Request $request, UserPasswordEncoderInterface $password_encoder)
    {
        $user = new User();
        $form = $this->createForm(UserFormType::class, $user, ['user' => true]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            try
            {
                $user->setPassword($password_encoder->encodePassword(
                    $user,
                    $user->getPassword()
                ));
                if ($file = $request->files->get('user_form')['file_image'])
                {
                    $this->dir_to_save = "/public/users/profile";
                    $user->setImage($this->upload($file));
                }

                $this->getEM()->persist($user);

                $this->getEM()->flush();
                $this->addFlash('success', 'El nuevo usuario fue añadido exitosamente.');

                return $this->redirectToRoute('app_user');
            }
            catch (\Exception $exception)
            {
                error_log($exception->getMessage());
                //Send flash message
                $this->addFlash('error', 'Existe un usuario con ese Email ' . $user->getEmail());
            }
        }

        return $this->render('backend/user/new.html.twig', [
            'form' => $form->createView(),
            'show_email' => true,
        ]);
    }

    /**
     * @Route("/{id}/editar", name="app_edit_user")
     * @param User $user
     * @param Request $request
     * @param UserPasswordEncoderInterface $password_encoder
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function edit(User $user, Request $request, UserPasswordEncoderInterface $password_encoder)
    {
        $form = $this->createForm(UserFormType::class, $user, ['editing' => true]);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {
            try
            {
                /** @var User $user */
                $user = $form->getData();

                $user->setPassword($password_encoder->encodePassword(
                    $user,
                    $user->getPassword()
                ));

                $user->setRoles([$request->request->get('user_form')['roles']]);

                //Upload and process of Imagen
                if ($file = $request->files->get('user_form')['file_image'])
                {
                    $this->dir_to_save = "/public/users/profile";
                    $user->setImage($this->upload($file));
                }

                if ($file = $request->files->get('user_form')['file_image'])
                {
                    $this->dir_to_save = "/public/users/profile";
                    $user->setImage($this->upload($file));
                }

                $this->getEM()->persist($user);

                $this->getEM()->flush();

                $this->addFlash('success', 'El usuario fue actualizado exitosamente.');
                return $this->redirectToRoute('app_user');
            }
            catch (\Exception $exception)
            {
                error_log($exception->getMessage());
                //Send flash message
                $this->addFlash('error', 'Existe un usuario con ese Email '. $user->getEmail());
            }
        }

        return $this->render('backend/user/edit.html.twig', [
            'form' => $form->createView(),
            'user' => $user,
            'show_email' => true,
        ]);
    }

    /**
     * @Route("/{id}", name="app_remove_user")
     * @param Request $request
     * @param User $user
     * @return Response
     */
    public function remove(Request $request, User $user) : Response
    {
        if ($user)
        {
            try
            {
                $this->getEM()->remove($user);
                $this->getEM()->flush();
                $this->addFlash('success', 'El usuario ' . $user->getUsername() . ' fue eliminado exitosamente');
            }
            catch (\Exception $exception)
            {
                error_log( $exception->getMessage() );
                $this->addFlash('error', 'Ocurrió un error al eliminar el usuario ' . $user->getUsername());
            }
        }
        else
        {
            throw $this->createNotFoundException('No se encontro el usuario a eliminar.');
        }

        return $this->redirectToRoute('app_user');
    }

    /**
     * @Route("/{id}/perfil", name="app_profile_user")
     *
     * @param User $user
     * @param Request $request
     * @param UserPasswordEncoderInterface $password_encoder
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function profile(User $user, Request $request, UserPasswordEncoderInterface $password_encoder)
    {
        $role = $user->getRoles();
        $form = $this->createForm(UserFormType::class, $user, ['profile' => true]);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {
            try
            {
                $user = $form->getData();
                $user->setPassword($password_encoder->encodePassword(
                    $user,
                    $user->getPassword()
                ));
                $user->setRoles($role);
                $this->getEM()->persist($user);

                $this->getEM()->flush();

                $this->addFlash('success', 'Su perfil fue actualizado exitosamente.');
                return $this->redirectToRoute('dashboard');
            }
            catch (\Exception $exception)
            {
                error_log($exception->getMessage());
                //Sending flash message
                $this->addFlash('error', 'Existe un usuario con ese Email ' . $user->getEmail());
            }
        }

        return $this->render('backend/user/profile.html.twig', [
            'form' => $form->createView(),
            'user' => $user,
            'show_email' => false,
        ]);
    }

}
