<?php

namespace App\Controller\Backend;

use App\Repository\ConfigRepository;
use App\Repository\UserRepository;
use App\Repository\VisitorRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class AdminController
 * @package App\Controller
 * @Route("/admin")
 */
class AdminController extends BaseController {

    /**
     * @Route("/", name="admin_start")
     */
    public function start()
    {
        return $this->redirectToRoute("admin_dashboard");
    }

    /**
     * @Route("/dashboard", name="admin_dashboard")
     *
     * @param UserRepository $userRepository
     * @param BusinessRepository $businessRepository
     * @param ServiceRepository $serviceRepository
     * @param ConfigRepository $configRepository
     * @param VisitorRepository $visitorRepository
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(UserRepository $userRepository, ConfigRepository $configRepository,
            VisitorRepository $visitorRepository )
    {

        if (in_array('ROLE_CLIENT', $this->getUser()->getRoles())) {
            return $this->redirectToRoute('logout');
        }

        list($labels, $values) = $visitorRepository->findVisitInLabelAndValue();

        return $this->render('backend/admin/dashboard.html.twig', [
                    'users' => $userRepository->count(['enabled' => true]),
            'visitors' => empty($config = $configRepository->findOneBy(['name' => 'visitors'])) ? 0 : $config->getContent(),
                    'labels' => json_encode($labels, 1),
            'values' => json_encode($values, 1),
        ]);
    }

}
