<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\FaqRepository;
use App\Repository\TermOfUseRepository;
use App\Entity\Contact;
use App\Form\ContactType;

class FrontendController extends AbstractController {

    /**
     * @Route("/", name="homepage")
     * @return Response
     */
    public function index(Request $request) {
        return $this->render('frontend/index.html.twig', ['active' => 'homepage']);
    }

    /**
     * @Route("/solid", name="solid")
     * @return Response
     */
    public function solid() {
        return $this->render('frontend/solid.html.twig', ['active' => 'solid']);
    }

    /**
     * @Route("/faq", name="faq")
     * @return Response
     */
    public function faq(FaqRepository $faqRepository) {

        $faqs = $faqRepository->findAll();

        return $this->render('frontend/faq.html.twig', ['active' => 'faq', 'faqs' => $faqs]);
    }

    /**
     * @Route("/contact", name="contact")
     * @return Response
     */
    public function contact(Request $request): Response {
        $contact = new Contact();
        $form = $this->createForm(ContactType::class, $contact);
        $form->handleRequest($request);

        $em = $this->getDoctrine()->getManager();

        if ($form->isSubmitted() && $form->isValid()) {

            $em->persist($contact);
            $em->flush();

            $contact->setTicket("SAAC" . str_pad($contact->getId(), 4, "0", STR_PAD_LEFT));
            $em->flush();

            $this->addFlash('success', 'Hemos recibido su mensaje. En breve atenderemos su inquietud.');

            return $this->redirectToRoute('homepage');
        }

        return $this->render('frontend/contact.html.twig', [
                    'active' => 'contact',
                    'contact' => $contact,
                    'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/terms-of-use", name="terms_of_use")
     * @return Response
     */
    public function termsOfUse(TermOfUseRepository $touRepository) {

        $tous = $touRepository->findAllOrdered();

        return $this->render('frontend/tou.html.twig', ['active' => '', 'tous' => $tous]);
    }

    /**
     * @Route("/cookies-policy", name="cookies_policy")
     * @return Response
     */
    public function cookiesPolicy() {

        return $this->render('frontend/cookies_policy.html.twig', ['active' => '']);
    }

}
