<?php

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use App\Classes\StringUtil;

class AppExtension extends AbstractExtension {

    public function getFilters() {
        return [
            new TwigFilter('roman', [$this, 'numberToRoman']),
        ];
    }

    /**
     * @param int $number
     * @return string
     */
    function numberToRoman($number) {
        return StringUtil::numberToRoman($number);
    }

}
