<?php

namespace App\Repository;

use App\Entity\TermOfUse;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TermOfUse|null find($id, $lockMode = null, $lockVersion = null)
 * @method TermOfUse|null findOneBy(array $criteria, array $orderBy = null)
 * @method TermOfUse[]    findAll()
 * @method TermOfUse[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TermOfUseRepository extends ServiceEntityRepository {

    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, TermOfUse::class);
    }

    // /**
    //  * @return TermOfUse[] Returns an array of TermOfUse objects
    //  */
    /*
      public function findByExampleField($value)
      {
      return $this->createQueryBuilder('t')
      ->andWhere('t.exampleField = :val')
      ->setParameter('val', $value)
      ->orderBy('t.id', 'ASC')
      ->setMaxResults(10)
      ->getQuery()
      ->getResult()
      ;
      }
     */

    public function findAllOrdered(): ?array {
        return $this->createQueryBuilder('t')
                        ->orderBy('t.number')
                        ->getQuery()
                        ->getResult()
        ;
    }

}
