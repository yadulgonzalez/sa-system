<?php

namespace App\Repository;

use App\Entity\Account;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use App\Repository\PaymentRepository;

/**
 * @method Account|null find($id, $lockMode = null, $lockVersion = null)
 * @method Account|null findOneBy(array $criteria, array $orderBy = null)
 * @method Account[]    findAll()
 * @method Account[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AccountRepository extends ServiceEntityRepository
 {

    private $paymentRepository;

    public function __construct(ManagerRegistry $registry, PaymentRepository $paymentRepository) {
        parent::__construct($registry, Account::class);

        $this->paymentRepository = $paymentRepository;
    }

    public function update(Account $account, float $balanceCredit = 0, float $balanceDebit = 0) {

        $activeContracts = 0;
        $activeCapital = 0;
        $bestPlan = null;

        for ($c = 0; $c < $account->getContracts()->count(); $c++) {

            $contract = $account->getContracts()->get($c);

            if ($contract->isActive()) {
                $activeContracts++;
                $activeCapital += $contract->getPlan()->getValue();
            }
            $compare = $contract->getPlan()->compareTo($bestPlan);
            if ($compare == 1) {
                $bestPlan = $contract->getPlan();
            }
        }
        $account->setActiveContracts($activeContracts);
        $account->setActiveCapital($activeCapital);

        $retained = $this->paymentRepository->getRetainedValue($account);

        $account->setRetained(!empty($retained) ? $retained : 0);

        $account->updateBalance($balanceCredit, $balanceDebit);

        $account->setBestPlan($bestPlan);

        $this->getEntityManager()->flush();
    }

}
