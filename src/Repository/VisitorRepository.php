<?php

namespace App\Repository;

use App\Entity\Visitor;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Visitor|null find($id, $lockMode = null, $lockVersion = null)
 * @method Visitor|null findOneBy(array $criteria, array $orderBy = null)
 * @method Visitor[]    findAll()
 * @method Visitor[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class VisitorRepository extends ServiceEntityRepository
{
    private $months = ['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'];

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Visitor::class);
    }

    public function findVisitInLabelAndValue()
    {
        $visitors = $this->createQueryBuilder('v')
            ->select('YEAR(v.date) as y, MONTH(v.date) as m, SUM(v.visit) as visit')
            ->groupBy('y')
            ->addGroupBy('m')
            ->orderBy('y', 'ASC')
            ->addOrderBy('m', 'ASC')
            ->getQuery()
            ->getResult()
        ;
        $labels = [];
        $values = [];

        foreach ($visitors as $visitor)
        {
            $labels[] = $this->months[$visitor['m']-1] . ' ' . $visitor['y'];
            $values[] = $visitor['visit'];
        }

        return [$labels, $values];
    }
}
