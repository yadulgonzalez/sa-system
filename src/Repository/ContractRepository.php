<?php

namespace App\Repository;

use App\Entity\Contract;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Contract|null find($id, $lockMode = null, $lockVersion = null)
 * @method Contract|null findOneBy(array $criteria, array $orderBy = null)
 * @method Contract[]    findAll()
 * @method Contract[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ContractRepository extends ServiceEntityRepository {

    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, Contract::class);
    }

    public function generateNextCode(): ?string {

        $lastRecord = $this->createQueryBuilder('c')
                ->select('MAX(c.code) AS code')
                ->getQuery()
                ->getSingleScalarResult();

        $dateCode = date('y');

        if (empty($lastRecord)) {
            return $dateCode . '00001';
        }

        $counter = (int) substr($lastRecord, 2);

        return $dateCode . str_pad($counter + 1, 5, '0', STR_PAD_LEFT);
    }

}
