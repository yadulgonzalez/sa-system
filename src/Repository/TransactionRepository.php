<?php

namespace App\Repository;

use App\Entity\Transaction;
use App\Entity\Account;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Transaction|null find($id, $lockMode = null, $lockVersion = null)
 * @method Transaction|null findOneBy(array $criteria, array $orderBy = null)
 * @method Transaction[]    findAll()
 * @method Transaction[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TransactionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Transaction::class);
    }

    public function findLastRecord(int $count) {

        $query = $this->createQueryBuilder('t')
                ->orderBy('t.date', 'DESC')
                ->setMaxResults($count)
                ->getQuery();

        return $count == 1 ? $query->getOneOrNullResult() : $query->getResult();
    }

    public function create(Account $account, $credit, $debit, $concept): ?Transaction {

        $last = $this->findLastRecord(1);

        $balance = !empty($last) ? $last->getBalance() + $credit - $debit : $credit - $debit;

        $trans = new Transaction($account, $credit, $debit, $balance, $concept);

        $this->getEntityManager()->persist($trans);
        $this->getEntityManager()->flush();

        return $trans;
    }

}
