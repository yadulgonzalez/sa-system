<?php

namespace App\Repository;

use App\Entity\Payment;
use App\Entity\Account;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Payment|null find($id, $lockMode = null, $lockVersion = null)
 * @method Payment|null findOneBy(array $criteria, array $orderBy = null)
 * @method Payment[]    findAll()
 * @method Payment[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PaymentRepository extends ServiceEntityRepository {

    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, Payment::class);
    }

    public function getRetainedValue(Account $account): ?float {

        return $this->createQueryBuilder('p')
                        ->select('SUM(p.value)')
                        ->where('p.retained = :ret')
                        ->andWhere('p.released = :rel')
                        ->andWhere('p.account = :acc')
                        ->setParameter('ret', true)
                        ->setParameter('rel', false)
                        ->setParameter('acc', $account)
                        ->getQuery()
                        ->getSingleScalarResult();
    }

}
