<?php

namespace App\Entity;

use App\Repository\ContractRepository;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Plan;
use App\Entity\Account;
use App\Classes\DateUtil;

/**
 * @ORM\Entity(repositoryClass=ContractRepository::class)
 */
class Contract {

    public const NETWORK_PERCENT = [
        1 => 0.08,
        2 => 0.03,
        3 => 0.01
    ];

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=12)
     */
    private $code;

    /**
     * @ORM\Column(type="date")
     */
    private $purchaseDate;

    /**
     * @ORM\Column(type="date")
     */
    private $startDate;

    /**
     * @ORM\Column(type="date")
     */
    private $finishDate;

    /**
     * @ORM\ManyToOne(targetEntity=Plan::class, inversedBy="contracts")
     * @ORM\JoinColumn(nullable=false)
     */
    private $plan;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $paymentMethod;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $paymentCurrency;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    private $networkFee;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    private $operatingCapital;

    /**
     * @ORM\ManyToOne(targetEntity=Account::class, inversedBy="contracts")
     * @ORM\JoinColumn(nullable=false)
     */
    private $account;

    public function __construct($code, Account $account, Plan $plan, $paymentMethod, $paymentCurrency) {
        $this->code = $code;
        $this->plan = $plan;
        $this->account = $account;
        $this->paymentMethod = $paymentMethod;
        $this->paymentCurrency = $paymentCurrency;

        $this->purchaseDate = date_create_from_format("Y-m-d", date("Y-m-d"));
        $this->startDate = clone($this->purchaseDate);
        $this->startDate->add(new \DateInterval('P2D'));
        $this->finishDate = clone($this->startDate);
        $this->finishDate->add(new \DateInterval('P8M'));

        $this->operatingCapital = $plan->getValue();
        $this->networkFee = 0;

        if (!empty($account->getUser()->getMySponsor())) {
            $this->networkFee += Contract::NETWORK_PERCENT[1];
        }
        if (!empty($account->getUser()->getMySponsor()) && !empty($account->getUser()->getMySponsor()->getMySponsor())) {
            $this->networkFee += Contract::NETWORK_PERCENT[2];
        }
        if (!empty($account->getUser()->getMySponsor()) && !empty($account->getUser()->getMySponsor()->getMySponsor()) && !empty($account->getUser()->getMySponsor()->getMySponsor()->getMySponsor())) {
            $this->networkFee += Contract::NETWORK_PERCENT[3];
        }
        $this->operatingCapital = $plan->getValue() * (1 - $this->networkFee);
    }

    public function isActive(): ?bool {

        $today = DateUtil::getTodayExactDate();

        return $this->purchaseDate <= $today && $this->finishDate >= $today;
    }

    public function isPayable(): ?bool {

        $today = DateUtil::getTodayExactDate();

        return $this->startDate <= $today && $this->finishDate >= $today;
    }

    public function getId(): ?int {
        return $this->id;
    }

    public function getCode(): ?string {
        return $this->code;
    }

    public function setCode(string $code): self {
        $this->code = $code;

        return $this;
    }

    public function getPurchaseDate(): ?\DateTimeInterface {
        return $this->purchaseDate;
    }

    public function setPurchaseDate(\DateTimeInterface $purchaseDate): self {
        $this->purchaseDate = $purchaseDate;

        return $this;
    }

    public function getStartDate(): ?\DateTimeInterface {
        return $this->startDate;
    }

    public function setStartDate(\DateTimeInterface $startDate): self {
        $this->startDate = $startDate;

        return $this;
    }

    public function getFinishDate(): ?\DateTimeInterface {
        return $this->finishDate;
    }

    public function setFinishDate(\DateTimeInterface $finishDate): self {
        $this->finishDate = $finishDate;

        return $this;
    }

    public function getPlan(): ?Plan {
        return $this->plan;
    }

    public function setPlan(?Plan $plan): self {
        $this->plan = $plan;

        return $this;
    }

    public function getPaymentMethod(): ?string {
        return $this->paymentMethod;
    }

    public function setPaymentMethod(string $paymentMethod): self {
        $this->paymentMethod = $paymentMethod;

        return $this;
    }

    public function getPaymentCurrency(): ?string {
        return $this->paymentCurrency;
    }

    public function setPaymentCurrency(string $paymentCurrency): self {
        $this->paymentCurrency = $paymentCurrency;

        return $this;
    }

    public function getNetworkFee(): ?string {
        return $this->networkFee;
    }

    public function setNetworkFee(string $networkFee): self {
        $this->networkFee = $networkFee;

        return $this;
    }

    public function getOperatingCapital(): ?string {
        return $this->operatingCapital;
    }

    public function setOperatingCapital(string $operatingCapital): self {
        $this->operatingCapital = $operatingCapital;

        return $this;
    }

    public function getAccount(): ?Account {
        return $this->account;
    }

    public function setAccount(?Account $account): self {
        $this->account = $account;

        return $this;
    }

}
