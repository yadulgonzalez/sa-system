<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiProperty;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *     normalizationContext={"groups"={"read"}},
 *     denormalizationContext={"groups"={"write"}},
 *     collectionOperations={
 *          "post"= {
 *              "path"= "/login"
 *          }
 *     }
 * )
 */
final class Login {

    /**
     * @ApiProperty(identifier=true)
     */
    public function getId() {
        return 0;
    }

    /**
     * @var string
     * @Groups("read")
     */
    private $token;

    /**
     * @var integer
     * @Groups("read")
     */
    private $reconfigure;

    public function __construct() {
        $this->token = "";
        $this->reconfigure = 0;
    }

    public function getToken(): string {
        return $this->token;
    }

    public function setToken(string $token): void {
        $this->token = $token;
    }

    public function getReconfigure(): int {
        return $this->reconfigure;
    }

    public function setReconfigure(int $reconfigure): void {
        $this->reconfigure = $reconfigure;
    }

}
