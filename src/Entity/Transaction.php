<?php

namespace App\Entity;

use App\Repository\TransactionRepository;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Account;

/**
 * @ORM\Entity(repositoryClass=TransactionRepository::class)
 */
class Transaction {

    public function __construct(Account $account, $credit, $debit, $balance, $concept) {
        $this->date = new \DateTime();
        $this->account = $account;
        $this->credit = $credit;
        $this->debit = $debit;
        $this->balance = $balance;
        $this->concept = $concept;
    }

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Account::class, inversedBy="transactions")
     * @ORM\JoinColumn(nullable=false)
     */
    private $account;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    private $credit;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    private $debit;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    private $balance;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $concept;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date;

    public function getId(): ?int {
        return $this->id;
    }

    public function getAccount(): ?Account {
        return $this->account;
    }

    public function setAccount(?Account $account): self {
        $this->account = $account;

        return $this;
    }

    public function getCredit(): ?string {
        return $this->credit;
    }

    public function setCredit(string $credit): self {
        $this->credit = $credit;

        return $this;
    }

    public function getDebit(): ?string {
        return $this->debit;
    }

    public function setDebit(string $debit): self {
        $this->debit = $debit;

        return $this;
    }

    public function getBalance(): ?string {
        return $this->balance;
    }

    public function setBalance(string $balance): self {
        $this->balance = $balance;

        return $this;
    }

    public function getConcept(): ?string {
        return $this->concept;
    }

    public function setConcept(string $concept): self {
        $this->concept = $concept;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

}
