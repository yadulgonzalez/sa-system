<?php

namespace App\Entity;

use App\Repository\PlanRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Classes\StringUtil;

/**
 * @ORM\Entity(repositoryClass=PlanRepository::class)
 */
class Plan {

    public const COLORS = [
        'background' => [
            1 => 'bg-plan-starter',
            2 => 'bg-plan-medium',
            3 => 'bg-plan-advanced',
            4 => 'bg-plan-expert'
        ],
        'border' => [
            1 => 'bg2-plan-starter',
            2 => 'bg2-plan-medium',
            3 => 'bg2-plan-advanced',
            4 => 'bg2-plan-expert'
        ]
    ];

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $name;

    /**
     * @ORM\Column(type="integer")
     */
    private $level;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    private $value;

    /**
     * @ORM\Column(type="integer")
     */
    private $priority;

    /**
     * @ORM\OneToMany(targetEntity=Account::class, mappedBy="bestPlan")
     */
    private $accounts;

    /**
     * @ORM\OneToMany(targetEntity=Contract::class, mappedBy="plan")
     */
    private $contracts;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    private $minPayment;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    private $maxPayment;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    private $avePayment;

    public function __construct()
    {
        $this->accounts = new ArrayCollection();
        $this->contracts = new ArrayCollection();
    }

    public function getCompleteName() {
        return $this->name . ' ' . StringUtil::numberToRoman($this->level);
    }

    public function compareTo(Plan $other = null): ?int {

        if (empty($other) || ($this->priority > $other->getPriority())) {
            return 1;
        } elseif ($this->priority < $other->getPriority()) {
            return -1;
        }
        return 0;
    }

    public function getId(): ?int {
        return $this->id;
    }

    public function getName(): ?string {
        return $this->name;
    }

    public function setName(string $name): self {
        $this->name = $name;

        return $this;
    }

    public function getLevel(): ?int {
        return $this->level;
    }

    public function setLevel(int $level): self {
        $this->level = $level;

        return $this;
    }

    public function getValue(): ?string {
        return $this->value;
    }

    public function setValue(string $value): self {
        $this->value = $value;

        return $this;
    }

    public function getPriority(): ?int {
        return $this->priority;
    }

    public function setPriority(int $priority): self {
        $this->priority = $priority;

        return $this;
    }

    /**
     * @return Collection|Account[]
     */
    public function getAccounts(): Collection
    {
        return $this->accounts;
    }

    public function addAccount(Account $account): self
    {
        if (!$this->accounts->contains($account)) {
            $this->accounts[] = $account;
            $account->setBestPlan($this);
        }

        return $this;
    }

    public function removeAccount(Account $account): self
    {
        if ($this->accounts->contains($account)) {
            $this->accounts->removeElement($account);
            // set the owning side to null (unless already changed)
            if ($account->getBestPlan() === $this) {
                $account->setBestPlan(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Contract[]
     */
    public function getContracts(): Collection
    {
        return $this->contracts;
    }

    public function addContract(Contract $contract): self
    {
        if (!$this->contracts->contains($contract)) {
            $this->contracts[] = $contract;
            $contract->setPlan($this);
        }

        return $this;
    }

    public function removeContract(Contract $contract): self
    {
        if ($this->contracts->contains($contract)) {
            $this->contracts->removeElement($contract);
            // set the owning side to null (unless already changed)
            if ($contract->getPlan() === $this) {
                $contract->setPlan(null);
            }
        }

        return $this;
    }

    public function getMinPayment(): ?string
    {
        return $this->minPayment;
    }

    public function setMinPayment(string $minPayment): self
    {
        $this->minPayment = $minPayment;

        return $this;
    }

    public function getMaxPayment(): ?string
    {
        return $this->maxPayment;
    }

    public function setMaxPayment(string $maxPayment): self
    {
        $this->maxPayment = $maxPayment;

        return $this;
    }

    public function getAvePayment(): ?string
    {
        return $this->avePayment;
    }

    public function setAvePayment(string $avePayment): self
    {
        $this->avePayment = $avePayment;

        return $this;
    }

}
