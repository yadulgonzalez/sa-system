<?php

namespace App\Entity;

use App\Repository\AccountRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=AccountRepository::class)
 */
class Account {

    public const COLORS = [
        1 => '#c0dad4',
        2 => '#9e573c',
        3 => '#9a9fa9',
        4 => '#d2b800'
    ];

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity=User::class, inversedBy="account", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    private $balance;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    private $retained;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    private $activeCapital;

    /**
     * @ORM\Column(type="integer")
     */
    private $activeContracts;

    /**
     * @ORM\ManyToOne(targetEntity=Plan::class, inversedBy="accounts")
     */
    private $bestPlan;

    /**
     * @ORM\OneToMany(targetEntity=Contract::class, mappedBy="account")
     */
    private $contracts;

    /**
     * @ORM\OneToMany(targetEntity=Transaction::class, mappedBy="account")
     */
    private $transactions;

    /**
     * @ORM\OneToMany(targetEntity=Payment::class, mappedBy="account")
     */
    private $payments;

    public function __construct(User $user) {
        $this->user = $user;
        $this->activeCapital = 0;
        $this->activeContracts = 0;
        $this->balance = 0;
        $this->retained = 0;
        $this->bestPlan = null;
        $this->contracts = new ArrayCollection();
        $this->transactions = new ArrayCollection();
        $this->payments = new ArrayCollection();
    }

    public function updateBalance(float $balanceCredit = 0, float $balanceDebit = 0) {
        $this->balance += $balanceCredit - $balanceDebit;
    }

    public function getId(): ?int {
        return $this->id;
    }

    public function getUser(): ?User {
        return $this->user;
    }

    public function setUser(User $user): self {
        $this->user = $user;

        return $this;
    }

    public function getBalance(): ?string {
        return $this->balance;
    }

    public function setBalance(string $balance): self {
        $this->balance = $balance;

        return $this;
    }

    public function getRetained(): ?float {
        return $this->retained;
    }

    public function setRetained(float $retained): self {
        $this->retained = $retained;

        return $this;
    }

    public function getActiveCapital(): ?float {
        return $this->activeCapital;
    }

    public function setActiveCapital(float $activeCapital): self {
        $this->activeCapital = $activeCapital;

        return $this;
    }

    public function getActiveContracts(): ?int {
        return $this->activeContracts;
    }

    public function setActiveContracts(int $activeContracts): self {
        $this->activeContracts = $activeContracts;

        return $this;
    }

    public function getBestPlan(): ?Plan {
        return $this->bestPlan;
    }

    public function setBestPlan(?Plan $bestPlan): self {
        $this->bestPlan = $bestPlan;

        return $this;
    }

    public function getColor(): ?string {

        return empty($this->bestPlan) ? 'transparent' : Account::COLORS[$this->bestPlan->getPriority()];
    }

    /**
     * @return Collection|Contract[]
     */
    public function getContracts(): Collection {
        return $this->contracts;
    }

    public function addContract(Contract $contract): self {
        if (!$this->contracts->contains($contract)) {
            $this->contracts[] = $contract;
            $contract->setAccount($this);
        }

        return $this;
    }

    public function removeContract(Contract $contract): self {
        if ($this->contracts->contains($contract)) {
            $this->contracts->removeElement($contract);
            // set the owning side to null (unless already changed)
            if ($contract->getAccount() === $this) {
                $contract->setAccount(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Transaction[]
     */
    public function getTransactions(): Collection {
        return $this->transactions;
    }

    public function addTransaction(Transaction $transaction): self {
        if (!$this->transactions->contains($transaction)) {
            $this->transactions[] = $transaction;
            $transaction->setAccount($this);
        }

        return $this;
    }

    public function removeTransaction(Transaction $transaction): self {
        if ($this->transactions->contains($transaction)) {
            $this->transactions->removeElement($transaction);
            // set the owning side to null (unless already changed)
            if ($transaction->getAccount() === $this) {
                $transaction->setAccount(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Payment[]
     */
    public function getPayments(): Collection {
        return $this->payments;
    }

    public function addPayment(Payment $payment): self {
        if (!$this->payments->contains($payment)) {
            $this->payments[] = $payment;
            $payment->setAccount($this);
        }

        return $this;
    }

    public function removePayment(Payment $payment): self {
        if ($this->payments->contains($payment)) {
            $this->payments->removeElement($payment);
            // set the owning side to null (unless already changed)
            if ($payment->getAccount() === $this) {
                $payment->setAccount(null);
            }
        }

        return $this;
    }

}
