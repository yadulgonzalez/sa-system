<?php

namespace App\Entity;

class Register {

    private $name;
    private $email;
    private $email2;
    private $password;
    private $password2;

    public function getName() {
        return $this->name;
    }

    public function getEmail() {
        return $this->email;
    }

    public function getEmail2() {
        return $this->email2;
    }

    public function getPassword() {
        return $this->password;
    }

    public function getPassword2() {
        return $this->password2;
    }

    public function setName($name): void {
        $this->name = $name;
    }

    public function setEmail($email): void {
        $this->email = $email;
    }

    public function setEmail2($email2): void {
        $this->email2 = $email2;
    }

    public function setPassword($password): void {
        $this->password = $password;
    }

    public function setPassword2($password2): void {
        $this->password2 = $password2;
    }

}
