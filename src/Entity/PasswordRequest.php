<?php
/**
 * Created by PhpStorm.
 * User: TONYSOFT
 * Date: 16/09/2020
 * Time: 15:26
 */

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;

/**
 * @ApiResource(
 *     collectionOperations={
 *         "post"={
 *              "path"="/credentials"
 *         }
 *     },
 *     itemOperations={},
 *     output=false
 * )
 */
final class PasswordRequest {

    /**
     * @var string
     */
    public $data;

}
