<?php

namespace App\DataPersister;

use ApiPlatform\Core\DataPersister\ContextAwareDataPersisterInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use App\Repository\ConfigRepository;
use App\Repository\UserRepository;
use App\Entity\Login;

/**
 * Description of LoginPersister
 *
 * @author yadul
 */
final class LoginPersister implements ContextAwareDataPersisterInterface {

    protected $requestStack;
    protected $configRepository;
    protected $userRepository;

    public function __construct(RequestStack $requestStack, ConfigRepository $configRepository, UserRepository $userRepository) {

        $this->requestStack = $requestStack;
        $this->configRepository = $configRepository;
        $this->userRepository = $userRepository;
    }

    public function supports($data, array $context = []): bool {
        return $data instanceof Login;
    }

    public function persist($data, array $context = []) {

        $request = $this->requestStack->getCurrentRequest();

        $data = json_decode($request->getContent(), true);

        //Find the user
        $user = $this->userRepository->findOneBy(['email' => $data['email']]);

        $login = new Login();

        $login->setToken($user->getToken());
        $login->setReconfigure($request->headers->get("X-Config"));

        if
        (
                !empty($config = $this->configRepository->findOneBy(['name' => 'categories_status'])) &&
                $config->getContent() != $login->getReconfigure()
        )
        {
            $login->setReconfigure($config->getContent());
        }

        return $login;
    }

    public function remove($data, array $context = []) {
        // call your persistence layer to delete $data
    }

}
