<?php
namespace App\DataPersister;

use ApiPlatform\Core\DataPersister\ContextAwareDataPersisterInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use App\Repository\UserRepository;
use App\Entity\PasswordRequest;

/**
 * Description of PasswordRequestPersister
 *
 * @author yadul
 */
class PasswordRequestPersister implements ContextAwareDataPersisterInterface {

    protected $requestStack;
    protected $userRepository;

    public function __construct(RequestStack $requestStack, UserRepository $userRepository) {

        $this->requestStack = $requestStack;
        $this->userRepository = $userRepository;
    }

    public function persist($data, array $context = []) {

        $request = $this->requestStack->getCurrentRequest();

        $token = substr($request->headers->get("Authorization"), 7);

        //Find the user
        $user = $this->userRepository->findOneByToken($token);

        if (!empty($user)) {
            //TODO: Enviar email con la nueva clave
        }

        return $data;
    }

    public function remove($data, array $context = []) {

    }

    public function supports($data, array $context = []): bool {
        return $data instanceof PasswordRequest;
    }

}
