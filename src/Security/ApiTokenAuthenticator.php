<?php

namespace App\Security;

use App\Classes\EmailSender;
use App\Entity\User;
use App\Repository\ConfigRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Container\ContainerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;
use Twig\Environment;

class ApiTokenAuthenticator extends AbstractGuardAuthenticator
{

    private $user_repository;
    private $user_password_encoder;
    private $manager;
    private $twig;
    private $container;
    private $parameters_bag;
    private $config_repository;

    function __construct(UserRepository $user_repository, Environment $twig, ContainerInterface $container,
                         UserPasswordEncoderInterface $user_password_encoder, EntityManagerInterface $manager,
                         ConfigRepository $configRepository, ContainerBagInterface $bag)
    {
        $this->user_repository = $user_repository;
        $this->user_password_encoder = $user_password_encoder;
        $this->manager = $manager;
        $this->twig = $twig;
        $this->container = $container;
        $this->parameters_bag = $bag;
        $this->config_repository = $configRepository;
    }

    public function supports(Request $request)
    {

        //return $request->headers->has('Authorization')
        //    && 0 === strpos($request->headers->get('Authorization'),'Bearer');

        return 0 === strpos(strtolower($request->attributes->get('_route')), 'api');
    }

    public function getCredentials(Request $request)
    {

        if ("api_logins_post_collection" === strtolower($request->attributes->get('_route'))) {
            return [
                'action' => 'login',
                'request' => json_decode($request->getContent(), true),
            ];
        }
        elseif ("api_registers_post_collection" === strtolower($request->attributes->get('_route'))) {
            return [
                'action' => 'register',
                'request' => json_decode($request->getContent(), true),
            ];
        }

        $autorization_header = $request->headers->get('Authorization');

        return $autorization_header ? substr($autorization_header, 7) : false;
    }

    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        if (empty($credentials))
        {
            throw new CustomUserMessageAuthenticationException('Acceso denegado.');
        }
        elseif (is_array($credentials))
        {
            if ('login' === $credentials['action'])
            {
                if
                (
                    !empty($user = $this->user_repository->findOneBy(['email' => $credentials['request']['email']]))
                    &&
                    $this->user_password_encoder->isPasswordValid($user, $credentials['request']['password'])
                    &&
                    $user->getEnabled()
                )
                {
                    return $user;
                }

                throw new CustomUserMessageAuthenticationException('Usuario inactivo o no autorizado.');
            }
            if ('register' === $credentials['action'])
            {
                if (!filter_var($credentials['request']['email'], FILTER_VALIDATE_EMAIL)) {
                    throw new CustomUserMessageAuthenticationException('Dirección de email incorrecta.');
                }
                if (empty($credentials['request']['name'])) {
                    throw new CustomUserMessageAuthenticationException('Se requiere el nombre del usuario.');
                }
                if (empty($credentials['request']['password'])) {
                    throw new CustomUserMessageAuthenticationException('Se requiere la contraseña.');
                }
                if (empty($credentials['request']['role'])) {
                    throw new CustomUserMessageAuthenticationException('Acceso denegado.');
                }

                if (empty($user = $this->user_repository->findOneBy(['email' => $credentials['request']['email']])))
                {
                    $user = new User();
                    $user->setEmail($credentials['request']['email']);
                    $user->setFirstName($credentials['request']['name']);
                    $user->setUserName($credentials['request']['name']);
                    $user->setRoles([$credentials['request']['role']]);
                    $user->setPassword($this->user_password_encoder->encodePassword($user, $credentials['request']['password']));
                    $user->setEnabled(0);
                    $user->setCreatedAt(new \DateTime());

                    $this->manager->persist($user);
                    $this->manager->flush();

                    //Create the content
                    $content = $this->twig->render('frontend/emails_templates/active_user.html.twig',
                        [
                            'user' => $user,
                        ]
                    );

                    $subject = "Activar su usuario en TocToc Servicios Cuba";
                    //Sending email
                    EmailSender::sendEmail( $this->container, $this->parameters_bag, $this->config_repository,null, $user->getEmail(), $subject, $content );

                    return $user;
                }

                throw new CustomUserMessageAuthenticationException('Existe un usuario con ese email.');
            }
        }

        $user = $this->user_repository->findOneByToken($credentials);

        if (empty($user)) {
            throw new CustomUserMessageAuthenticationException('Credencial de API no válida.');
        }

        if (!$user->getEnabled()) {
            throw new CustomUserMessageAuthenticationException('Usuario inhabilitado.');
        }

        return $user;
    }

    public function checkCredentials($credentials, UserInterface $user)
    {
        return true;
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        return new JsonResponse([
            'message' => $exception->getMessageKey()
        ]);
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, string $providerKey)
    {
        // allow the request to continue
    }

    public function start(Request $request, AuthenticationException $authException = null)
    {
        throw new \Exception('Not Used');
    }

    public function supportsRememberMe()
    {
        return false;
    }
}
