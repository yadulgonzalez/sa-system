<?php

/**
 * Created by PhpStorm.
 * User: TONYSOFT
 * Date: 15/09/2020
 * Time: 00:28
 */
namespace App\DTO;


final class ServiceOutput
{
    /**
     * @var integer
     */
    public $id;

    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $description;

    /**
     * @var float
     */
    public $price;

    /**
     * @var string
     */
    public $image;

    /**
     * @var boolean
     */
    public $active;

}