<?php

/**
 * Created by PhpStorm.
 * User: TONYSOFT
 * Date: 12/09/2020
 * Time: 21:01
 */
namespace App\DTO;


final class UserOutput
{
    /**
     * @var integer
     */
    public $id;

    /**
     * @var string
     */
    public $email;

    /**
     * @var string
     */
    public $first_name;

    /**
     * @var string
     */
    public $last_name;

    /**
     * @var string
     */
    public $image;

}