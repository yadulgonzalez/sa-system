<?php

/**
 * Created by YGM
 */

namespace App\DTO;


final class UserVoteOutput {

    /**
     * @var integer
     */
    public $likes;

    /**
     * @var integer
     */
    public $unlikes;

}
