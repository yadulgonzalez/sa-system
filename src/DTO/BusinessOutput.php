<?php

/**
 * Created by YGM
 */

namespace App\DTO;


final class BusinessOutput {
    /**
     * @var integer
     */
    public $id;

    /**
     * @var string
     */
    public $name;

    /**
     * @var text
     */
    public $description;

    /**
     * @var string
     */
    public $slogan;

    /**
     * @var integer
     */
    public $xconfig;

    /**
     * @var integer
     */
    public $likes;

    /**
     * @var integer
     */
    public $unlikes;

    /**
     * @var integer
     */
    public $likesToMe;

    /**
     * @var string
     */
    public $mainPhone;

    /**
     * @var string
     */
    public $province;

    /**
     * @var string
     */
    public $municipal;

    /**
     * @var boolean
     */
    public $showPrices;

    /**
     * @var boolean
     */
    public $isOpen;

}