<?php

/**
 * Created by PhpStorm.
 * User: TONYSOFT
 * Date: 12/09/2020
 * Time: 21:25
 */

namespace App\DataFixtures;

use App\Entity\EventType;
use Doctrine\Persistence\ObjectManager;

class EventTypeFixtures extends BaseFixture
{
    private $types = [
        "PRE_REGISTRO",
        "ENTRADA",
        "SALIDA",
        "ENTREVISTA_REALIZADA",
        "APROBACION",
        "CARGA_DE_DOCUMENTO",
        "DESCARGA_DE_DOCUMENTO",
        "RESETEO_DE_CLAVE",
        "CAMBIO_DE_CLAVE",
        "NUEVO_USUARIO",
        "USUARIO_ACTIVADO",
        "USUARIO_DESACTIVADO",
        "NUEVA_MODALIDAD",
        "EDICION_MODALIDAD",
        "ACTIVAR_MODALIDAD",
        "DESACTIVAR_MODALIDAD",
        "NUEVA_CATEGORIA",
        "EDICION_CATEGORIA",
        "ACTIVAR_CATEGORIA",
        "DESACTIVAR_CATEGORIA",
        "RECUPERACION_DE_CLAVE",
        "TIPO_SERVICIO_NUEVO",
        "TIPO_SERVICIO_EDICION",
        "TIPO_SERVICIO_ACTIVAR",
        "TIPO_SERVICIO_DESACTIVAR",
        "ENTREVISTA_SOLICITADA",
        "ENTREVISTA_PROGRAMADA",
        "EVENTO_API_LOGIN",
        "EVENTO_API_LOGOUT",
    ];

    protected function loadData(ObjectManager $manager)
    {
        $this->createMany(EventType::class, count($this->types), function( $pos, $manager){
            $event_type= new EventType();
            $event_type->setName($this->types[$pos]);
            $event_type->setClass('fa fa-comment');
            $event_type->setColor('darkgreen');

            return $event_type;

        });

        $manager->flush();

    }
}
