<?php

/**
 * Created by PhpStorm.
 * User: TONYSOFT
 * Date: 12/09/2020
 * Time: 22:38
 */

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends BaseFixture {

    private $passwordEncoder;

    private static $first_names = ['Juan', 'Jose', 'Pedro', 'Luis', 'Alberto'];
    private static $last_names  = ['Pérez', 'Díaz', 'Valiente', 'Chacón', 'Rodríguez'];

    /**
     * UserFixtures constructor.
     * @param UserPasswordEncoderInterface $passwordEncoder
     */
    public function __construct(UserPasswordEncoderInterface $passwordEncoder) {
        $this->passwordEncoder = $passwordEncoder;
    }

    /**
     * @param ObjectManager $manager
     */
    protected function loadData( ObjectManager $manager )
    {
        //Set Admin
        $user = new User();
        $user->setUsername( 'admin' );
        $user->setEmail('admin@solid-assets.com');
        $user->setFirstName('Administrador');
        $user->setLastName('');
        $user->setEnabled( true );
        $user->setCreatedAt( $this->faker->dateTimeBetween('-30 days', '-2 days') );
        $user->setRoles( ['ROLE_ADMIN'] );
        $user->setPassword($this->passwordEncoder->encodePassword($user, 'admin'));
        $manager->persist($user);

        //Set Client
        $user = new User();
        $user->setUsername( 'client' );
        $user->setEmail('client@gmail.com');
        $user->setFirstName( $this->faker->randomElement(self::$first_names) );
        $user->setLastName( $this->faker->randomElement(self::$last_names) );
        $user->setEnabled( true );
        $user->setCreatedAt( $this->faker->dateTimeBetween('-30 days', '-2 days') );
        $user->setRoles( ['ROLE_CLIENT'] );
        $user->setPassword( $this->passwordEncoder->encodePassword($user,'client') );
        $manager->persist($user);

        $manager->flush();
    }

}
