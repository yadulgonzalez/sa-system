<?php

namespace App\DataFixtures;

use App\Entity\Config;
use Doctrine\Persistence\ObjectManager;

class ConfigFixtures extends BaseFixture {
    private $names = ['service_status'];
    private $values = [1];

    protected function loadData(ObjectManager $manager)
    {
        $this->createMany(Config::class, count($this->names), function ($pos, $manager) {
            $config = new \App\Entity\Config();
            $config->setName($this->names[$pos]);
            $config->setContent($this->values[$pos]);

            return $config;
        });

        $manager->flush();
    }
}
