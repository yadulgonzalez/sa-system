<?php

namespace App\DQLFunctions;

use Doctrine\ORM\Query\AST\Functions\FunctionNode;
use Doctrine\ORM\Query\Lexer;

class RoundFunction extends FunctionNode
{
    public $valor;
    public $decimales = 2;

    /**
     * @param \Doctrine\ORM\Query\SqlWalker $sqlWalker
     * @return string
     */
    public function getSql(\Doctrine\ORM\Query\SqlWalker $sqlWalker)
    {
        return "ROUND(" . $sqlWalker->walkArithmeticPrimary($this->valor) . ",". $sqlWalker->walkArithmeticPrimary($this->decimales) . ")";
    }

    /**
     * @param \Doctrine\ORM\Query\Parser $parser
     */
    public function parse(\Doctrine\ORM\Query\Parser $parser)
    {
        $parser->match(Lexer::T_IDENTIFIER);
        $parser->match(Lexer::T_OPEN_PARENTHESIS);
        $this->valor = $parser->ArithmeticPrimary();
        $parser->match(Lexer::T_COMMA);
        $this->decimales = $parser->ArithmeticPrimary();
        $parser->match(Lexer::T_CLOSE_PARENTHESIS);
    }

}