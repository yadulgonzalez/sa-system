#Toc Toc Servicios Cuba - API

##
##Envío de mensajes
##

URL: <server path>/api/messages

La petición se realiza con los siguientes requisitos:
	- Method: POST
	- Authentication: Bearer Token
	- ContentType: application-json
	- Body: 
			{
				"intent": <command>,
				"destination": <user IRI>,
				"message": <JSON body>
			}

Ej. 
			{
				"intent": "bussget",
				"destination": "/api/users/0",
				"message": "{\"category\":9}"
			}

En el ejemplo anterior se envía una petición de los negocios disponibles en la categoría Restaurantes (9)
al usuario SYSTEM (0). Este usuario debe estar creado por defecto como motor del sistema.

INSERT INTO `toctoc`.`user`(`id`, `username`, `enabled`) VALUES (0, 'system', 0);
UPDATE `toctoc`.`user` SET `id`=0 WHERE `username` = 'system';

##
##Recepción de mensajes pendientes para un usuario
##

URL: <server path>/api/messages

La petición se realiza con los siguientes requisitos:
	- Method: GET
	- Authentication: Bearer Token

Se devuelven todos los mensajes no leidos que tienen como destino el token enviado. 
Los mensajes devueltos son marcados como enviados y serán ignorados en las siguientes peticiones.


##Login

Para la petición del login se usará la url:

```
http://localhost/toctoc/public/api/login
```

Y el payload siguiente como body de la petición:

```
{
    "email": "pepe@toctoc.nat.cu",
    "password" : "pepe",
    "categories_status" : "0"
}
```

Si la respuesta de la petición no es exitosa se devolverá un string JSON como sigue:

```
{
    "message": "Usuario inactivo o no autorizado."
}
```

Si fue exitosa la petición se devolverá una respuesta como se muestra a continuación:

```
{
    "token": "2a41a6d144327514227eae90314eaeaf641afb5ba3bd957114c708807b36d29c1a79c452b4f80b646d0f3d696cbc0f5b7a9e0031e6643c1781d9311b",
    "reconfigure": 1,
    "categories": []
}
```


##Registrarse

En el caso de registrarse la petición se hará a la url:

```
<server path>/api/register
```

Y el payload siguiente como body de la petición:

```
{
    "name" : "mike",
    "email": "mike@toctoc.nat.cu",
    "password" : "mike",
    "role" : "ROLE_CLIENT"
}
```

Si la respuesta de la petición no es exitosa se devolverá un string JSON como sigue:

```
{
    "message": "Existe un usuario con ese email."
}
```

Si fue exitosa la petición se devolverá una respuesta como se muestra a continuación:

```
{
    "status": 1
}
```


##Reseteo de contraseña

En el caso de reseteo la petición se hará a la url:

```
<server path>/api/credentials
```

Y el payload siguiente como body de la petición:

```
{
    "message" : ""
}
```

La petición no devolverá valor alguno.


## Petición de negocios

```
URL: <server path>/api/business
```

La petición se realiza con los siguientes requisitos:
	- Method: GET
	- Authentication: Bearer Token
	- ContentType: application-json
	- X-Category: id de la categoría a que pertenecen

La petición devolverá un arreglo con los negocios que pertenecen a la categoría especificada en el parámetro X-Category.
Si no hay negocios registrados, devolverá un arreglo vacío. 

